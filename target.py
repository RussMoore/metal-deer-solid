import pygame as pg
import math

class Target():
    def __init__(self):
        self.x = 0
        self.y = 0

    def update(self, screen, x_shift, mouse=None):
        if mouse:
            self.x = mouse[0] + x_shift
            self.y = mouse[1]
            pg.draw.line(screen, 'red', (mouse[0]-25, mouse[1]-25), (mouse[0]+25,mouse[1]+25), 7)
            pg.draw.line(screen, 'red', (mouse[0]-25, mouse[1]+25), (mouse[0]+25,mouse[1]-25), 7)

class Bullet:
    def __init__(self, character, mouse, x_shift):
        aim = (mouse[0]+x_shift, mouse[1])
        x_i = character.x_pos + character.width//2
        y_i = character.y_pos + character.height//2
        self.x = x_i
        self.y = y_i
        raw_vel = 5
        distance = ((aim[0]-x_i)**2+(aim[1]-y_i)**2)**(.5)
        time = distance/raw_vel
        self.vels = ((aim[0]-self.x)/time, (aim[1]-self.y)/time)
        print(f'velocities: x: {self.vels[0]}, y: {self.vels[1]}')
        print(f'distances: x: {self.x-aim[0]}, y: {self.y-aim[1]}')

    def update(self, screen, target_coors, x_shift):
        tc = (target_coors[0], target_coors[1])
        self.x += self.vels[0]
        self.y += self.vels[1]
        if self.y > screen.get_width():
            pass
        if ((self.x-tc[0])**2+(self.y-tc[1])**2)**(.5) < 20:
            print(f'target hit at {self.x}, {self.y}')
            pass
        print(self.x, self.y)
        pg.draw.circle(screen, 'black', (self.x-x_shift,self.y), 5)
