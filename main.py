import pygame as pg
import my_drawings
from characters import Moveable, MainChar, Deer
from target import Target, Bullet


pg.init()
# flags = pg.OPENGL | pg.FULLSCREEN
screen = pg.display.set_mode(size=(1280,960))
clock = pg.time.Clock()
running = True

test_char = MainChar(100, screen.get_height()//3-50)
deer1 = Deer(800, (screen.get_height()*2)//3, 100, 270, 100)
deer2 = Deer(1200, (screen.get_height()*2)//3, 100, 270, 700)
deer_list = [deer1, deer2]
mouse = Target()
x_shift = 0
bullet_list = []

while running:

    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
        if event.type == pg.KEYDOWN:
            if event.key == pg.K_RIGHT:
                test_char.go('right')
            elif event.key == pg.K_c:
                test_char.change_stance('prone')
            elif event.key == pg.K_LEFT:
                test_char.go('left')
            elif event.key == pg.K_b:
                print(pg.mouse.get_pos())
            elif event.key == pg.K_n:
                print("NEW DEER*****************")
            elif event.key == pg.K_SPACE:
                bullet_list.append(Bullet(test_char, pg.mouse.get_pos(), x_shift))
                #print(test_char.x_pos,test_char.y_pos, 0, pg.mouse.get_pos())


        elif event.type == pg.KEYUP:
            if event.key == pg.K_RIGHT or event.key == pg.K_LEFT:
                test_char.x_vel = 0
            elif event.key == pg.K_c:
                test_char.change_stance('standing')

    #move characters:
    test_char.move()



    #calculate if x shift needs to move depending on where in screen char is

    if test_char.x_pos+test_char.width - x_shift <= screen.get_width()*3//8:
        x_shift = test_char.x_pos+test_char.width - screen.get_width()*3//8
    elif x_shift + screen.get_width()*5//8 <= test_char.x_pos:
        x_shift = test_char.x_pos - screen.get_width()*5//8


    screen.fill("#f0745c")
    my_drawings.sun(screen, x_shift)
    my_drawings.meadow(screen)
    my_drawings.river(screen, x_shift)
    test_char.draw(screen, x_shift)
    for deer in deer_list:

        deer.update(screen, x_shift)
    mouse.update(screen, x_shift, pg.mouse.get_pos())
    for index in range(len(bullet_list)):
        bullet_list[index].update(screen, (1142-x_shift,642), x_shift)
        # if bullet_list[index].z >1000:
        #print(bullet.x, bullet.y, bullet.z)

    pg.display.flip()
    clock.tick(60)

pg.quit()
