import my_drawings
import pygame as pg


class Moveable():
    def __init__(self, x_pos, y_pos, height, width):
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.height = height
        self.width = width
        self.draw_function = my_drawings.draw_test
        self.x_vel = 0
        self.stance = 'standing'
        self.dir = 'right'
        pass


    def test_draw(self, screen, x_shift):
        if self.x_pos - x_shift > screen.get_width():
            return None
        self.draw_function(screen, x_shift, object=self)


    def move(self):
        self.x_pos += self.x_vel


    def go(self, dir):
        if dir == 'left':
            mult = -1
            print(self.dir, dir, self.dir == 'right', self.x_pos, self.width)
            if self.dir == 'right':
                self.x_pos -= self.width
                self.dir = dir
        elif dir == 'right':
            mult = 1
            if self.dir == 'left':
                self.x_pos += self.width
                self.dir = dir
        if self.stance == 'standing':
            self.x_vel = mult*2
        if self.stance == 'prone':
            self.x_vel = mult*1


class MainChar(Moveable):
    def __init__(self, x_pos, y_pos):
        super().__init__(x_pos, y_pos, 100, 40)

    def draw(self, screen, x_shift):
        #if self.stance == 'standing':
        left = self.x_pos - x_shift
        w = self.width
        top = self.y_pos
        h = self.height
        color = '#151c18'
        pg.draw.rect(screen, color,(left,top,w,h))
        if self.stance == 'standing':
            pg.draw.circle(screen, color, (left+w//2, top-w//2), w//2)
        elif self.stance == 'prone' and self.dir == 'right':
            pg.draw.circle(screen, color, (left+h//2+w, top+h//4), h//2)
        elif self.stance == 'prone' and self.dir == 'left':
            pg.draw.circle(screen, color, (left-h//2, top+h//4), h//2)




    def change_stance(self, stance):
        if self.stance == 'standing' and stance == 'prone':
            if self.x_vel > 0:
                self.x_vel = 1
            elif self.x_vel < 0:
                self.x_vel = -1
            self.y_pos += (self.height-self.width)
            w = self.width
            self.width = self.height
            self.height = w
            if self.dir == 'left':
                self.x_pos -= (self.width-w)  # prone left should move head
        elif self.stance == 'prone' and stance == 'standing':  # from prone to stand
            if self.x_vel > 0:
                self.x_vel = 2
            elif self.x_vel < 0:
                self.x_vel = -2
            self.y_pos -= (self.width-self.height)
            w = self.width
            self.width = self.height
            self.height = w
            if self.dir == 'left':
                self.x_pos += (self.height-self.width)
        self.stance = stance


class Deer(Moveable):
    def __init__(self, x_pos, y_pos, height, width, deer_pos):
        super().__init__(x_pos, y_pos, height, width)
        self.behavior = 'grazing'  # grazing, alert, seek, ignore, walk
        self.behaviors = ('grazing', 'alert', 'grazing', 'ignore', 'walk')
        self.behavior_timer = 0
        self.deer_pos = deer_pos


    def change_behavior(self):
        if self.behavior == self.behaviors[-1]:
            self.behavior = self.behaviors[0]
        else:
            self.behavior = self.behaviors[self.behaviors.index(self.behavior)+1]

    def update(self, screen, x_shift):
        if self.behavior_timer > 60*10:
            self.behavior_timer = 0
            self.change_behavior()
        self.behavior_timer += 1
        self.draw(screen, x_shift)
        print(self.behavior_timer, self.behavior)



    def draw(self, screen, x_shift):
        d_p = self.deer_pos
        #if self.stance == 'standing':
        left = self.x_pos - x_shift
        w = self.width
        top = self.y_pos
        h = self.height
        color1 = '#b28b78'
        # pg.draw.rect(screen, color1,(left,top,w,h))
        if self.behavior == 'grazing':
            pg.draw.polygon(screen, color1, ((567 - x_shift + d_p, 641),
                                            (821 - x_shift + d_p, 631),
                                            (835 - x_shift + d_p, 643),
                                            (838 - x_shift + d_p, 662),
                                            (839 - x_shift + d_p, 674),
                                            (832 - x_shift + d_p, 694),
                                            (809 - x_shift + d_p, 727),
                                            (812 - x_shift + d_p, 736),
                                            (839 - x_shift + d_p, 789),
                                            (803 - x_shift + d_p, 863),
                                            (792 - x_shift + d_p, 862),
                                            (808 - x_shift + d_p, 793),
                                            (763 - x_shift + d_p, 738),
                                            (610 - x_shift + d_p, 743),
                                            (592 - x_shift + d_p, 761),
                                            (565 - x_shift + d_p, 848),
                                            (551 - x_shift + d_p, 846),
                                            (566 - x_shift + d_p, 746),
                                            (556 - x_shift + d_p, 722),
                                            (563 - x_shift + d_p, 638),))
            pg.draw.polygon(screen, color1, ((563 - x_shift + d_p, 639),
                                            (524 - x_shift + d_p, 648),
                                            (501 - x_shift + d_p, 665),
                                            (481 - x_shift + d_p, 684),
                                            (451 - x_shift + d_p, 714),
                                            (432 - x_shift + d_p, 738),
                                            (421 - x_shift + d_p, 764),
                                            (406 - x_shift + d_p, 785),
                                            (381 - x_shift + d_p, 773),
                                            (382 - x_shift + d_p, 794),
                                            (380 - x_shift + d_p, 808),
                                            (387 - x_shift + d_p, 821),
                                            (394 - x_shift + d_p, 829),
                                            (396 - x_shift + d_p, 845),
                                            (396 - x_shift + d_p, 851),
                                            (408 - x_shift + d_p, 861),
                                            (419 - x_shift + d_p, 861),
                                            (429 - x_shift + d_p, 846),
                                            (437 - x_shift + d_p, 830),
                                            (444 - x_shift + d_p, 818),
                                            (463 - x_shift + d_p, 805),
                                            (470 - x_shift + d_p, 797),
                                            (491 - x_shift + d_p, 777),
                                            (519 - x_shift + d_p, 755),
                                            (544 - x_shift + d_p, 732),
                                            (555 - x_shift + d_p, 723),))
        elif self.behavior == 'alert':
            pg.draw.polygon(screen, color1, ((567 - x_shift + d_p, 641),
                                            (821 - x_shift + d_p, 631),
                                            (835 - x_shift + d_p, 643),
                                            (838 - x_shift + d_p, 662),
                                            (839 - x_shift + d_p, 674),
                                            (832 - x_shift + d_p, 694),
                                            (809 - x_shift + d_p, 727),
                                            (812 - x_shift + d_p, 736),
                                            (839 - x_shift + d_p, 789),
                                            (803 - x_shift + d_p, 863),
                                            (792 - x_shift + d_p, 862),
                                            (808 - x_shift + d_p, 793),
                                            (763 - x_shift + d_p, 738),
                                            (610 - x_shift + d_p, 743),
                                            (592 - x_shift + d_p, 761),
                                            (565 - x_shift + d_p, 848),
                                            (551 - x_shift + d_p, 846),
                                            (566 - x_shift + d_p, 746),
                                            (556 - x_shift + d_p, 722),
                                            (563 - x_shift + d_p, 638)))
            pg.draw.polygon(screen, color1, ((566 - x_shift + d_p, 641),
                                            (529 - x_shift + d_p, 621),
                                            (508 - x_shift + d_p, 603),
                                            (485 - x_shift + d_p, 587),
                                            (493 - x_shift + d_p, 563),
                                            (477 - x_shift + d_p, 572),
                                            (450 - x_shift + d_p, 572),
                                            (436 - x_shift + d_p, 580),
                                            (423 - x_shift + d_p, 587),
                                            (396 - x_shift + d_p, 599),
                                            (395 - x_shift + d_p, 623),
                                            (410 - x_shift + d_p, 631),
                                            (432 - x_shift + d_p, 635),
                                            (453 - x_shift + d_p, 647),
                                            (492 - x_shift + d_p, 669),
                                            (528 - x_shift + d_p, 699),
                                            (553 - x_shift + d_p, 724),))
