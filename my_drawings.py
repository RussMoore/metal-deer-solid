import pygame as pg

def river(screen, x_shift):
    left_point = 300
    if left_point-x_shift> screen.get_width():
        return None
    top = screen.get_height() //3
    bot = screen.get_height()
    pg.draw.polygon(surface=screen, color='blue', points=((left_point+100-x_shift,top), (left_point+200-x_shift, top), (left_point+150-x_shift, bot), (left_point-x_shift, bot)))
    return None

def meadow(screen):
    top = screen.get_height() //3
    bot = screen.get_height()
    height = screen.get_height()-top
    width = screen.get_width()
    #rect is (left, top, width, height)
    pg.draw.rect(screen, "#094466", (0, top, width, height))

def sun(screen, x_shift):
    circle_pos = pg.Vector2(screen.get_width() // 3, screen.get_height() //3)
    pg.draw.circle(screen, '#cc1242', circle_pos, 50)

def character(screen, character):
    pass

def draw_test(screen, x_shift, object):
    left = object.x_pos - x_shift
    w = object.width
    top = object.y_pos
    h = object.height

    pg.draw.rect(screen, '#151c18',(left,top,w,h))
